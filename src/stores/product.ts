import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/Product";
import productService from "@/services/product";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useProductStore = defineStore("product", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();

  const dialog = ref(false);
  const deleteDialog = ref(false);
  const deleteItem = ref(-1);
  const products = ref<Product[]>([]);
  const editedProduct = ref<Product>({ name: "", price: 0 });
  watch(dialog, (newDialog, oldDialog) => {
    if (!newDialog) {
      editedProduct.value = { name: "", price: 0 };
    }
  });
  async function getProducts() {
    loadingStore.isLoading = true;
    try {
      const res = await productService.getProducts();
      products.value = res.data;
      console.log(res);
    } catch (e) {
      messageStore.showError("ไม่สามารถดึงข้อมูล product ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function saveProduct() {
    loadingStore.isLoading = true;
    try {
      if (editedProduct.value.id) {
        const res = await productService.updateProduct(
          editedProduct.value.id,
          editedProduct.value
        );
      } else {
        const res = await productService.saveProduct(editedProduct.value);
      }
      dialog.value = false;
      await getProducts();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก product ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function deleteProduct() {
    loadingStore.isLoading = true;
    try {
      const res = await productService.deleteProduct(deleteItem.value);
      await getProducts();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ product ได้");
    }
    deleteDialog.value = false;
    deleteItem.value = -1;
    loadingStore.isLoading = false;
  }

  function confirmDelete(id: number) {
    deleteItem.value = id;
    deleteDialog.value = true;
  }

  function closeConfirmDelete() {
    deleteItem.value = -1;
    deleteDialog.value = false;
  }

  function editProduct(product: Product) {
    editedProduct.value = JSON.parse(JSON.stringify(product));
    dialog.value = true;
  }

  return {
    products,
    getProducts,
    dialog,
    editedProduct,
    saveProduct,
    editProduct,
    deleteProduct,
    deleteDialog,
    confirmDelete,
    closeConfirmDelete,
  };
});
