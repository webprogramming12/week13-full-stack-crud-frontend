export default interface User {
  id?: number;
  name: string;
  userName: string;
  userPassword: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
